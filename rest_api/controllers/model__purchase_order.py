# -*- coding: utf-8 -*-
from .main import *

_logger = logging.getLogger(__name__)


# OUT data:
OUT__purchase_order__read_all__SUCCESS_CODE = 200       # editable
OUT__purchase_order__read_all__SCHEMA = (                 # editable
    'id',
    'name',
    'date_order',
    'state',
    ('partner_id', ('id', 'name')),

    ('order_line',
        [('id',
            'name',
            ('product_id', ('id', 'name')),
            ('product_uom', ('id', 'name')),)]),
)

OUT__purchase_order__read_one__SUCCESS_CODE = 200       # editable

OUT__purchase_order__read_one__SCHEMA = (                 # editable
    'id',
    'name',
    'date_order',
    'state',
    ('partner_id', ('id', 'name')),

    ('order_line',
        [('id',
            'name',
            ('product_id', ('id', 'name')),
            ('product_uom', ('id', 'name')),)]),

)


DEFAULTS__purchase_order__create_one__JSON = {          # editable
            #"some_field_1": some_value_1,
            #"some_field_2": some_value_2,
            #...
}

OUT__purchase_order__create_one__SUCCESS_CODE = 200     # editable
OUT__purchase_order__create_one__SCHEMA = (               # editable
    'id',
    'name',
    'date_order',
    'state',
    # ('partner_id', ('id', 'name')),

    # ('order_line',
    #     [('id',
    #         'name',
    #         ('product_id', ('id', 'name')),
    #         ('product_uom', ('id', 'name')),)]),
)
OUT__purchase_order__update_one__SUCCESS_CODE = 200     # editable
OUT__purchase_order__delete_one__SUCCESS_CODE = 200     # editable
OUT__purchase_order__call_method__SUCCESS_CODE = 200    # editable


# HTTP controller of REST resources:

class ControllerREST(http.Controller):
    
    # Read all (with optional filters, offset, limit, order, exclude_fields, include_fields):
    @http.route('/api/purchase.order', methods=['GET'], type='http', auth='none')
    @check_permissions
    def api__purchase_order__GET(self, **kw):
        return wrap__resource__read_all(
            modelname = 'purchase.order',
            default_domain = [],
            success_code = OUT__purchase_order__read_all__SUCCESS_CODE,
            OUT_fields = OUT__purchase_order__read_all__SCHEMA
        )
    
    # Read one (with optional exclude_fields, include_fields):
    @http.route('/api/purchase.order/<id>', methods=['GET'], type='http', auth='none')
    @check_permissions
    def api__purchase_order__id_GET(self, id, **kw):
        return wrap__resource__read_one(
            modelname = 'purchase.order',
            id = id,
            success_code = OUT__purchase_order__read_one__SUCCESS_CODE,
            OUT_fields = OUT__purchase_order__read_one__SCHEMA
        )
    
    # Create one:
    @http.route('/api/purchase.order', methods=['POST'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__purchase_order__POST(self, **kw):
        return wrap__resource__create_one(
            modelname = 'purchase.order',
            default_vals = DEFAULTS__purchase_order__create_one__JSON,
            success_code = OUT__purchase_order__create_one__SUCCESS_CODE,
            OUT_fields = OUT__purchase_order__create_one__SCHEMA
        )
    
    # Update one:
    @http.route('/api/purchase.order/<id>', methods=['PUT'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__purchase_order__id_PUT(self, id, **kw):
        return wrap__resource__update_one(
            modelname = 'purchase.order',
            id = id,
            success_code = OUT__purchase_order__update_one__SUCCESS_CODE
        )
    
    # Delete one:
    @http.route('/api/purchase.order/<id>', methods=['DELETE'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__purchase_order__id_DELETE(self, id, **kw):
        return wrap__resource__delete_one(
            modelname = 'purchase.order',
            id = id,
            success_code = OUT__purchase_order__delete_one__SUCCESS_CODE
        )
    
    # Call method (with optional parameters):
    @http.route('/api/purchase.order/<id>/<method>', methods=['PUT'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__purchase_order__id__method_PUT(self, id, method, **kw):
        return wrap__resource__call_method(
            modelname = 'purchase.order',
            id = id,
            method = method,
            success_code = OUT__purchase_order__call_method__SUCCESS_CODE
        )
