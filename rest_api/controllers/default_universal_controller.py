# -*- coding: utf-8 -*-
from .main import *

_logger = logging.getLogger(__name__)


class ControllerREST(http.Controller):
    
    # Read all (with optional filters, offset, limit, order, exclude_fields, include_fields):
    @http.route('/api/<string:model_name>', methods=['GET'], type='http', auth='none')
    @check_permissions
    def api__model_name__GET(self, model_name, **kw):
        cr, uid = request.cr, request.session.uid
        Model = request.env(cr, uid)[model_name]
        if 'name' in Model._fields.keys():
            OUT_fields = ('id', 'name',)
        else:
            OUT_fields = ('id',)
        return wrap__resource__read_all(
            modelname = model_name,
            default_domain = [],
            success_code = 200,
            OUT_fields = OUT_fields,
            pre_schema = False,
        )
    
    # Read one (with optional exclude_fields, include_fields):
    @http.route('/api/<string:model_name>/<id>', methods=['GET'], type='http', auth='none')
    @check_permissions
    def api__model_name__id_GET(self, model_name, id, **kw):
        cr, uid = request.cr, request.session.uid
        Model = request.env(cr, uid)[model_name]
        OUT_fields = tuple(Model._fields.keys())
        return wrap__resource__read_one(
            modelname = model_name,
            id = id,
            success_code = 200,
            OUT_fields = OUT_fields,
            pre_schema = False,
        )
    
    # Create one:
    @http.route('/api/<string:model_name>', methods=['POST'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__model_name__POST(self, model_name, **kw):
        return wrap__resource__create_one(
            modelname = model_name,
            default_vals = {},
            success_code = 200,
            OUT_fields = ('id',)
        )
    
    # Update one:
    @http.route('/api/<string:model_name>/<id>', methods=['PUT'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__model_name__id_PUT(self, model_name, id, **kw):
        return wrap__resource__update_one(
            modelname = model_name,
            id = id,
            success_code = 200,
        )
    
    # Delete one:
    @http.route('/api/<string:model_name>/<id>', methods=['DELETE'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__model_name__id_DELETE(self, model_name, id, **kw):
        return wrap__resource__delete_one(
            modelname = model_name,
            id = id,
            success_code = 200,
        )
    
    # Call method (with optional parameters):
    @http.route('/api/<string:model_name>/<id>/<method>', methods=['PUT'], type='http', auth='none', csrf=False)
    @check_permissions
    def api__model_name__id__method_PUT(self, model_name, id, method, **kw):
        return wrap__resource__call_method(
            modelname = model_name,
            id = id,
            method = method,
            success_code = 200,
        )
