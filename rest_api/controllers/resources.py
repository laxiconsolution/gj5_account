# -*- coding: utf-8 -*-
from . import auth

from . import model__account_invoice       # need install 'account' module
from . import model__account_invoice_line  # need install 'account' module
from . import model__product_template      # need install 'product' module
from . import model__report
from . import model__res_partner
from . import model__sale_order            # need install 'sale' module
from . import model__sale_order_line       # need install 'sale' module

from . import default_universal_controller
from . import model__product_product
from . import model__purchase_order
from . import model__account_tax
