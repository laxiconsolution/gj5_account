{
    # App information
    'name': 'Cancel Stock Picking',
    'version': '12.0',
    'category': 'stock',
    'summary': 'Allow to cancel Processed Picking.',
    'license': 'OPL-1',
    'author': 'Emipro Technologies Pvt. Ltd.',
    'website': 'http://www.emiprotechnologies.com',
    'maintainer': 'Emipro Technologies Pvt. Ltd.',
    'depends': ['stock', 'account_cancel'],
    'data': [
     'view/stock_picking.xml',
     'view/stock_location.xml'
    ],
    'demo': [],
    'images': ['static/description/cover-img.jpg'],
    'price': '59',
    'currency': 'EUR',
    'installable': True,
    'application': True,

}
