# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ServiceRequest(models.Model):
    _name = 'service.request'
    _description = 'Service Request Detail'

    name = fields.Char(string='Name')
    image = fields.Binary()
    sale_order_id = fields.Many2one('sale.order', string="Sale Order")
    partner_id = fields.Many2one('res.partner', string="Customer")
    email_from = fields.Char('Email', size=128, help="Destination email for email gateway.")
    phone = fields.Char('Phone')
    category = fields.Many2one('category.type', string="Category")
    description = fields.Text('Description')
    priority = fields.Selection([('0', 'Low'), ('1', 'Normal'), ('2', 'High')], 'Priority')
    support_team_id = fields.Many2one('support.team', string="Support")
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('progress', 'In Progress'),
            ('done', 'Done'),
            ('cancel', 'Cancel')], string="State", default='draft')

    @api.multi
    def action_send_email_service(self):
        self.ensure_one()
        service_template_id = self.env.ref('service_request_search.email_template_service_request').id
        compose_form_id = self.env.ref('mail.email_compose_message_wizard_form').id
        ctx = dict(
            default_composition_mode='comment',
            default_res_id=self.id,
            default_model='service.request',
            default_use_template=bool(service_template_id),
            default_template_id=service_template_id,
            custom_layout='mail.mail_notification_light'
        )
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.one
    def service_progress(self):
        for service in self:
            service.state = "progress"

    @api.one
    def service_done(self):
        for service in self:
            service.state = "done"

    @api.one
    def service_cancel(self):
        for service in self:
            service.state = "cancel"

    def service_whatsapp(self):
        record_phone = self.partner_id.mobile
        if not record_phone:
            view = self.env.ref('odoo_whatsapp_integration.warn_message_wizard')
            view_id = view and view.id or False
            context = dict(self._context or {})
            context['message'] = "Please add a mobile number!"
            return {
                'name': 'Mobile Number Field Empty',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'display.error.message',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': context
            }
        if not record_phone[0] == "+":
            view = self.env.ref('odoo_whatsapp_integration.warn_message_wizard')
            view_id = view and view.id or False
            context = dict(self._context or {})
            context['message'] = "No Country Code! Please add a valid mobile number along with country code!"
            return {
                'name': 'Invalid Mobile Number',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'display.error.message',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': context
            }
        else:
            return {'type': 'ir.actions.act_window',
                    'name': _('Whatsapp Message'),
                    'res_model': 'whatsapp.wizard',
                    'target': 'new',
                    'view_mode': 'form',
                    'view_type': 'form',
                    }


class Website(models.Model):

    _inherit = "website"

    def get_support_team_list(self):
        support_team_ids = self.env['support.team'].sudo().search([])
        return support_team_ids

    def get_category_type(self):
        ticket_type_ids = self.env['category.type'].sudo().search([])
        return ticket_type_ids
