# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class CategoryType(models.Model):
    _name = 'category.type'
    _description = "Category Type Detail"

    name = fields.Char('Category Name')
