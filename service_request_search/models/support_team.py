# -*- coding: utf-8 -*-

from odoo import models, fields


class SupportTeam(models.Model):
    _name = 'support.team'
    _description = 'Support Team Detail'

    name = fields.Char(string='Name')
    team_leader = fields.Many2one('res.users', string="Team Leader")
    user_id = fields.Many2one('res.users', string="Responsible")
