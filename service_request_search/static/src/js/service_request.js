odoo.define('service_request_search.service_request', function (require) {
    "use strict";

    $(function() {

        $(".oe_search_box").autocomplete({
            source: function(request, response) {
                $.ajax({
                url: "/service/search",
                method: "GET",
                dataType: "json",
                data: { keywords: request.term, category: false },
                success: function( data ) {               
                    response( $.map( data, function( item ) {
                        return {
                            label: (item.code),
                            value: item.code,
                        }
                    }));
                },
                error: function (error) {
                    console.error(error);               
                }
                });
            },
        });
    });
});