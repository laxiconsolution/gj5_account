{
    "name": "GJ5 Rest Api Connection",
    'description': """

    """,
    'author': "Laxicon Solution",
    'website': "www.laxicon.in",
    "version": "12.0.1",
    "category": "Manufaturing",
    "depends": ['base', 'sale_management', 'contacts'],
    "data": [
        'security/ir.model.access.csv',
        'views/branch_branch.xml',
        'views/res_partner.xml',
        'views/sale_order.xml',
        'views/product_template.xml',
    ],
    'sequence': 1,
    'installable': True,
    'auto_install': False,
    'application': True,
}
