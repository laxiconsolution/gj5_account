from odoo import fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    product_type = fields.Selection(
        [('rental', 'Rental'), ('fabric', 'Fabric'), ('readymade', 'Readymade'),
         ('perfume', 'Perfume'), ('novelty', 'Novelty')], string="Product Type", track_visibility='always')
