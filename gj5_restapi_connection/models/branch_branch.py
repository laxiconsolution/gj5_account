from odoo import fields, models
import requests
import json


class BranchBranch(models.Model):
    _name = 'branch.branch'
    _description = "Branch Detail"

    name = fields.Char(string="Name")
    is_showroom = fields.Boolean(string="IS ShowRoom")
    url = fields.Char(string="Url")
    user_name = fields.Char(string="User Name")
    password = fields.Char(string="Password")
    partner_id = fields.Many2one('res.partner', string="Partner")
    database_name = fields.Char(string="Database Name")
    access_token = fields.Char(string="Access Token", readonly=True)
    branch_uid = fields.Char(string="UID")

    def check_connection(self):
        if self.url and self.user_name and self.password and self.database_name:
            params = {'db': self.database_name, 'username': self.user_name, 'password': self.password}
            final_url = self.url + "/api/auth/get_tokens"
            response = requests.post(url=final_url, params=params)
            json_data = response.json()
            if response.status_code == 200:
                print ("json_data", json_data)
                self.access_token = json_data.get('access_token')
                self.branch_uid = json_data.get('partner_id')
            else:
                raise ValueError('please check server connection!')
        else:
            raise ValueError('please add Detail!')



# url = "http://localhost:8069"
# # db = "krackai"
# url = "http://letsmusic.io"
# db = "krack"
# username = "918871022432"
# password = "918871022432"

# # login with user (token)
# # {{host}}/api/auth/get_tokens?db=krackai&username=919977777887&password=919977777887
# final_url = url + "/api/auth/get_tokens"
# response = requests.post(url=final_url, params={'db': db, 'username': username, 'password': password})
# json_data = response.json()