from odoo import fields, models, api


class Respartner(models.Model):
    _inherit = 'res.partner'

    is_showroom = fields.Boolean(string='Is ShowRoom')
    branch_id = fields.Many2one('branch.branch', string="Branch")
 
    # def write(self, vals):
    #     if 'branch_id' in vals:
    #         self.branch_id.partner_id = self.id
    #     return super(Respartner, self).write(vals)
