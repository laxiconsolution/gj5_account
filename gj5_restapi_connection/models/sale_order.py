from odoo import fields, models, api
import requests
import json
from datetime import datetime
from odoo.exceptions import ValidationError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    branch_id = fields.Many2one('branch.branch', string="Branch", track_visibility='onchange')
    is_showroom_sale = fields.Boolean(string='Is ShowRoom Sale', track_visibility='onchange')
    branch_po_number = fields.Integer(string='Branch PoNumber', track_visibility='always')
    is_po_created = fields.Boolean(string="Is PO Create")
    order_type = fields.Selection(
        [('rental', 'Rental'), ('fabric', 'Fabric'), ('readymade', 'Readymade'),
         ('perfume', 'Perfume'), ('novelty', 'Novelty')], string="Order Type", track_visibility='always')

    def check_product_onshowroom(self, header):
        final_url = self.branch_id.url + "/api/product.product"
        for rec in self.order_line:
            if rec.product_id and rec.product_id.default_code:
                # domain = [('default_code', '=', rec.default_code), ('product_type', '=', rec.order_type)]
                domain = [('default_code', '=', rec.product_id.default_code)]
                if rec.order_id.order_type:
                    domain.append(('product_type', '=', rec.order_id.order_type))
                params = {'filters': str(domain)}
                response = requests.get(url=final_url, headers=header, params=params)
                response_data = response.json()
                count = response_data.get('count')
                if count == 1:
                    rec.write({
                        'b_product_id': response_data.get('results')[0].get('id'),
                        'b_product_uom_id': response_data.get('results')[0].get('uom_id').get('id')
                    })
                elif count == 0:
                    # uom get method
                    uom_url = self.branch_id.url + "/api/uom.uom"
                    domain = [('name', '=', rec.product_id.uom_id.name)]
                    params = {'filters': str(domain)}
                    uom_response = requests.get(url=uom_url, headers=header, params=params)
                    uom_count = uom_response.json().get('count')
                    if uom_count != 1:
                        raise ValidationError("Unit Of Measure Of Product Not Find In Showroom")
                    elif uom_count == 1:
                        uom_id = uom_response.json().get('results')[0].get('id')
                        product_dict = {
                            "name": rec.product_id.name,
                            "default_code": rec.product_id.default_code,
                            "sale_ok": 1,
                            "purchase_ok": 1,
                            "uom_id": uom_id,
                            'product_type': self.order_type,
                            'type': 'product'
                        }
                        # product template post method
                        create_url = self.branch_id.url + "/api/product.template"
                        product_tmpl_response = requests.post(url=create_url, headers=header, params=product_dict)
                        templ_id = product_tmpl_response.json().get('id')
                        if templ_id:
                            # product get method
                            product_url = self.branch_id.url + "/api/product.product"
                            domain = [('product_tmpl_id', '=', templ_id)]
                            params = {'filters': str(domain)}
                            product_response = requests.get(url=product_url, headers=header, params=params)
                            product_count = product_response.json().get('count')
                            if product_count != 1:
                                raise ValidationError("Product Template Id not match")
                            elif product_count == 1:
                                rec.write({
                                    'b_product_id': product_response.json().get('results')[0].get('id'),
                                    'b_product_uom_id': product_response.json().get('results')[0].get('uom_id').get('id')
                                })
                elif count > 1:
                    pass
                    # user error
                    raise ValidationError("Please Check same Multiple Product Found on This Show Room")

    def create_purchase_or(self, header):
        header.update({'Content-Type': "text/plain"})
        purchase_url = self.branch_id.url + "/api/purchase.order"
        po_dict = {"partner_id": int(self.branch_id.branch_uid), "partner_ref": str(self.name)}
        order_line = []
        for sale in self.order_line:
            data = {
                "name": sale.name,
                "product_id": sale.b_product_id,
                "product_qty": sale.product_uom_qty,
                "price_unit": sale.price_unit,
                "date_planned": str(datetime.now()),
                "product_uom": sale.b_product_uom_id,
                "company_id": 1
            }
            order_line.append(data)
        po_dict.update({"order_line": list(order_line)})
        po_response = requests.post(url=purchase_url, headers=header, json=po_dict)
        po_number = po_response.json().get('id')
        self.branch_po_number = po_number

    def call_api_function(self):
        if self.is_showroom_sale and self.branch_id:
            for res in self.order_line:
                if res.product_id and not res.product_id.default_code:
                    raise ValidationError('First Add Default Code In Product')
            self.branch_id.check_connection()
            header = {'Access-Token': self.branch_id.access_token}
            self.check_product_onshowroom(header)
            self.create_purchase_or(header)
            if self.branch_po_number:
                self.is_po_created = True

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        if self.partner_id:
            self.is_showroom_sale = True
            self.branch_id = self.partner_id.branch_id.id
            print ('ssssssssssss', self.branch_id)


class SaleOrderline(models.Model):
    _inherit = 'sale.order.line'

    b_product_id = fields.Integer(string="Branch Product", readonly=True)
    b_product_uom_id = fields.Integer(string='Branch UOM')
    branch_id = fields.Many2one(related='order_id.branch_id', string="Branch", store=True)
