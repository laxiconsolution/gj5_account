# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    partner_gst = fields.Char(string="Partner GSTIN")
    reversed_charged = fields.Selection([('y', 'Yes'), ('n', 'No')], string="Reverse Charge(Y/N)", default="n")

    @api.multi
    def number_to_words(self, n):
        words = ''

        units = ['', 'One', 'Two', 'Three', 'Four', 'Five',
                 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven',
                 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen',
                 'Seventeen', 'Eighteen', 'Nineteen']
        tens = ['', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty',
                'Sixty', 'Seventy', 'Eighty', 'Ninety']

        for group in ['', 'hundred', 'thousand', 'lac', 'crore']:

            if group in ['', 'thousand', 'lac']:
                n, digits = n // 100, n % 100
            elif group == 'hundred':
                n, digits = n // 10, n % 10
            else:
                digits = n

            if digits in range(1, 20):
                words = units[digits] + ' ' + group + ' ' + words
            elif digits in range(20, 100):
                ten_digit, unit_digit = digits // 10, digits % 10
                words = tens[ten_digit] + ' ' + units[unit_digit] + ' ' + group + ' ' + words
            elif digits >= 100:
                words = self.number_to_words(digits) + ' crore ' + words
        words = words + 'Rupees Only'
        return words

    @api.onchange('partner_id')
    def onchange_part_gst(self):
        if self.partner_id and self.partner_id.gst_number:
            self.partner_gst = self.partner_id.gst_number

    def get_type(self):
        tax_type = []
        for line in self.order_line:
            for tax in line.taxes_id:
                if tax.tax_type and tax.tax_type not in tax_type:
                    tax_type.append(str(tax.tax_type))
                elif not tax.tax_type and 'other' not in tax_type:
                    tax_type.append('other')
        return tax_type

    def get_tax_total(self):
        tax_total = {}
        for rec in self.order_line:
            price_unit = rec.price_unit
            for tax in rec.taxes_id:
                total = tax.compute_all(price_unit,
                                        self.currency_id,
                                        rec.product_qty,
                                        rec.product_id,
                                        self.partner_id)['taxes']
                for line in total:
                    if tax.tax_type:
                        if tax.tax_type not in tax_total:
                            tax_total.update({
                                            str(tax.tax_type): line['amount']
                                            })
                        else:
                            tax_total[str(tax.tax_type)] += line['amount']
                    else:
                        if 'other' not in tax_total:
                            tax_total.update({
                                        str('other'): line['amount']
                                        })
                        else:
                            tax_total[str('other')] += line['amount']
        return tax_total

    def get_line_total(self, line):
        for rec in line:
            price_total = line.price_subtotal
            for tax in rec.taxes_id:
                price_total = price_total + ((line.price_subtotal * tax.amount) / 100.0)
        return price_total

    def get_line_tax(self, line):
        invoice = line.order_id
        price_unit = line.price_unit

        taxes = line.taxes_id.compute_all(price_unit,
                                          invoice.currency_id,
                                          line.product_qty,
                                          line.product_id,
                                          invoice.partner_id)['taxes']
        tax_dict = {}
        for line in taxes:
            tax = self.env['account.tax'].sudo().browse(line['id'])
            if tax.tax_type and tax.tax_type not in tax_dict:
                tax_dict[tax.tax_type] = {}
            else:
                if 'other' not in tax_dict:
                    tax_dict['other'] = {}
            if tax.tax_type:
                tax_dict[tax.tax_type].update({
                    'rate': tax.amount,
                    'amount': line['amount'],
                    'tax_type': tax.tax_type,
                    'tax_id': tax,
                })
            else:
                tax_dict['other'].update({
                    'rate': tax.amount,
                    'amount': line['amount'],
                    'tax_type': 'other',
                    'tax_id': tax,
                })
        return tax_dict

    def get_lines(self):
        lines_by_tax_id = {}
        for line in self.order_line:
            if line.taxes_id:
                if line.taxes_id not in lines_by_tax_id:
                    lines_by_tax_id.update({line.taxes_id: {'lines': [], 'sum': 0.0}})
                lines_by_tax_id[line.product_id.taxes_id]['lines'].append(line)
                lines_by_tax_id[line.product_id.taxes_id]['sum'] += line.price_subtotal
                self.total += line.price_subtotal
        return lines_by_tax_id

    def get_amount_in_word(self):
        val = self.sudo().number_to_words(int(self.amount_total))
        return val

    def amount(self, line):
        total_amt = 0.0
        final = 0.0
        for rec in line:
            total_amt = (rec.price_unit * rec.product_qty)
            final = final + total_amt
        return final

    def total(self):
        total_amt = 0.0
        final = 0.0
        for rec in self.order_line:
            total_amt = (rec.price_unit * rec.product_qty)
            final = final + total_amt
        return final

    def get_total(self):
        return self.total


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    gst_id = fields.Many2one('product.gst', string='HSN Number')

    @api.onchange('product_id')
    def onchange_product_id(self):
        res = super(PurchaseOrderLine, self).onchange_product_id()
        gst_ids = {'domain': {'gst_id': [('id', 'in', self.product_id.categ_id.gst_ids.ids)]}}

        if self.product_id.gst_id:
            self.gst_id = self.product_id.gst_id
        else:
            if not isinstance(res, dict):
                res.update(gst_ids)
            else:
                if not res.get('domain', {}):
                    res.update({'domain': {}})
                res['domain'].update({'gst_id': [('id', 'in', self.product_id.categ_id.gst_ids.ids)]})

        return res

    @api.onchange('gst_id')
    def gst_id_change(self):
        customer_state_id = self.order_id.partner_id.state_id
        user_state_id = self.order_id.company_id.state_id
        if not self.order_id.partner_id:
            raise UserError(_("Please add the Vendor"))
        if customer_state_id == user_state_id:
            self.taxes_id = self.gst_id.cgst_purchase_tax_id + self.gst_id.sgst_purchase_tax_id
        else:
            self.taxes_id = self.gst_id.igst_purchase_tax_id


class ResCompany(models.Model):
    _inherit = 'res.company'

    @api.model
    def create(self, vals):
        res = super(ResCompany, self).create(vals)
        if res.id:
            seq_purchase_order = self.env.ref('purchase.seq_purchase_order')
            seq_purchase_order.sudo().copy(default={'company_id': res.id, 'number_next_actual': 1})
        return res
