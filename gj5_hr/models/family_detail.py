from odoo import api, fields, models


class HR(models.Model):
    _inherit = 'hr.employee'

    family_detail_ids = fields.One2many('hr.family.detail', 'employee_id', string="Family Detail")
    plan_display_by = fields.Char(string="Plan By")
    employee_refence = fields.Char(string="Reference")
    is_working_partner = fields.Boolean(string="Working Partner")
    is_salesperson_partner = fields.Boolean(string="Sales person Partner")
    is_investor_partner = fields.Boolean(string="Investor Partner")
    blood_grp = fields.Selection([
        ('A-', 'A-'),
        ('A+', 'A+'),
        ('B-', 'B-'),
        ('B+', 'B+'),
        ('O-', 'O-'),
        ('O+', 'O+'),
        ('AB-', 'AB-'),
        ('AB+', 'AB+'),
        ], string="Blood Group")
    

class FamilyDetail(models.Model):
    _name = 'hr.family.detail'
    _description = "Employee Family Detail"

    name = fields.Char(string="Name")
    employee_id = fields.Many2one('hr.employee', string="Employee")
    rela_with_parent = fields.Selection([
        ('self', 'Self'),
        ('grandfather', 'Grandfather'),
        ('grandmother', 'Grandmother'),
        ('father', 'Father'),
        ('monther', 'Mother'),
        ('uncle', 'Uncle'),
        ('aunty', 'Aunty'),
        ('husband', 'Hdsband'),
        ('wife', 'Wife'),
        ('sister', 'Sister'),
        ('brother', 'Brother'),
        ('daughter', 'Daughter'),
        ('son', 'Son'),
        ], string="Relation With employee")
    dob = fields.Date(string="DOB")
    primary_contact = fields.Char(string='Contact Number')
    alter_contact = fields.Char(string='Alternat Contact Number')
    email = fields.Char(string='Email Id')
    profile_pic = fields.Binary(string="Profile Picture")
    resi_add = fields.Char(string="Residency Address")
    blood_grp = fields.Selection([
        ('A-', 'A-'),
        ('A+', 'A+'),
        ('B-', 'B-'),
        ('B+', 'B+'),
        ('O-', 'O-'),
        ('O+', 'O+'),
        ('AB-', 'AB-'),
        ('AB+', 'AB+'),
        ], string="Blood Group")

    marital_status = fields.Selection([
        ('married', 'Married'),
        ('unmarried', 'Unmarried'),
        ], string="Marital Status")
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female')
        ], string='Gender')
    nominee = fields.Boolean(string="Nominee?")
    nominee_per = fields.Float(string="%")
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
