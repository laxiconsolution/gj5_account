# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
{
    "name": "HR for GJ5",
    "summary": "hr",
    'description': """

    """,
    'author': "Laxicon Solution",
    'website': "www.laxicon.in",
    "version": "12.0.1",
    "category": "Manufaturing",
    "depends": ['hr'],
    "data": [
        'security/ir.model.access.csv',
        'views/employee_family.xml'
    ],
    'sequence': 1,
    'installable': True,
    'auto_install': False,
    'application': True,
}
