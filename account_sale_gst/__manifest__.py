# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
{
    'name': "GST Sale Invoice",
    'summary': """GST Sale Invoice""",
    'description': """
GST for Sale purpose and report

Indian Accounting
GST
Indian GST
GST Report
GST Tax
Sale GST Process

    """,
    'author': "Laxicon Solution",
    'website': "www.laxicon.in",
    'price': 10.0,
    'sequence': 1,
    'currency': 'EUR',
    'category': 'Sale and Accounting',
    'version': '1.0',
    'depends': ['sale', 'account_invoice_gst', 'sale_management'],
    'images': ['static/description/gst_logo.png'],
    'data': [
        'views/sale_order_line_view.xml',
        'report/gst_sale_reg.xml',
        'report/gst_sale_order_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
