# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
# -*- coding: utf-8 -*-

import base64
from io import BytesIO
import xlsxwriter
from odoo import api, models, fields, _


class AccountFinancialReport(models.Model):
    _name = "account.financial.report"
    _description = "Account Report"

    @api.multi
    @api.depends('parent_id', 'parent_id.level')
    def _get_level(self):
        '''Returns a dictionary with key=the ID of a record and value = the level of this
           record in the tree structure.'''
        for report in self:
            level = 0
            if report.parent_id:
                level = report.parent_id.level + 1
            report.level = level

    def _get_children_by_order(self):
        '''returns a recordset of all the children computed recursively, and sorted by sequence. Ready for the printing'''
        res = self
        children = self.search([('parent_id', 'in', self.ids)], order='sequence ASC')
        if children:
            for child in children:
                res += child._get_children_by_order()
        return res

    name = fields.Char('Report Name', required=True, translate=True)
    parent_id = fields.Many2one('account.financial.report', 'Parent')
    children_ids = fields.One2many('account.financial.report', 'parent_id', 'Account Report')
    sequence = fields.Integer('Sequence')
    level = fields.Integer(compute='_get_level', string='Level', store=True)
    type = fields.Selection([
        ('sum', 'View'),
        ('accounts', 'Accounts'),
        ('account_type', 'Account Type'),
        ('account_report', 'Report Value'),
        ], 'Type', default='sum')
    account_ids = fields.Many2many('account.account', 'account_account_financial_report', 'report_line_id', 'account_id', 'Accounts')
    account_report_id = fields.Many2one('account.financial.report', 'Report Value')
    account_type_ids = fields.Many2many('account.account.type', 'account_account_financial_report_type', 'report_id', 'account_type_id', 'Account Types')
    sign = fields.Selection([(-1, 'Reverse balance sign'), (1, 'Preserve balance sign')], 'Sign on Reports', required=True, default=1,
                            help='For accounts that are typically more debited than credited and that you would like to print as negative amounts in your reports, you should reverse the sign of the balance; e.g.: Expense account. The same applies for accounts that are typically more credited than debited and that you would like to print as positive amounts in your reports; e.g.: Income account.')
    display_detail = fields.Selection([
        ('no_detail', 'No detail'),
        ('detail_flat', 'Display children flat'),
        ('detail_with_hierarchy', 'Display children with hierarchy')
        ], 'Display details', default='detail_flat')
    style_overwrite = fields.Selection([
        (0, 'Automatic formatting'),
        (1, 'Main Title 1 (bold, underlined)'),
        (2, 'Title 2 (bold)'),
        (3, 'Title 3 (bold, smaller)'),
        (4, 'Normal Text'),
        (5, 'Italic Text (smaller)'),
        (6, 'Smallest Text'),
        ], 'Financial Report Style', default=0,
        help="You can set up here the format you want this record to be displayed. "
             "If you leave the automatic formatting, it will be computed based on the "
             "financial reports hierarchy (auto-computed field 'level').")

    def _compute_account_balance(self, accounts):
        """ compute the balance, debit and credit for the provided accounts
        """
        mapping = {
            'balance': "COALESCE(SUM(debit),0) - COALESCE(SUM(credit), 0) as balance",
            'debit': "COALESCE(SUM(debit), 0) as debit",
            'credit': "COALESCE(SUM(credit), 0) as credit",
        }

        res = {}
        for account in accounts:
            res[account.id] = dict.fromkeys(mapping, 0.0)
        if accounts:
            tables, where_clause, where_params = self.env['account.move.line']._query_get()
            tables = tables.replace('"', '') if tables else "account_move_line"
            wheres = [""]
            if where_clause.strip():
                wheres.append(where_clause.strip())
            filters = " AND ".join(wheres)
            request = "SELECT account_id as id, " + ', '.join(mapping.values()) + \
                       " FROM " + tables + \
                       " WHERE account_id IN %s " \
                            + filters + \
                       " GROUP BY account_id"
            params = (tuple(accounts._ids),) + tuple(where_params)
            self.env.cr.execute(request, params)
            for row in self.env.cr.dictfetchall():
                res[row['id']] = row
        return res

    def _compute_report_balance(self, reports):
        '''returns a dictionary with key=the ID of a record and value=the credit, debit and balance amount
           computed for this record. If the record is of type :
               'accounts' : it's the sum of the linked accounts
               'account_type' : it's the sum of leaf accoutns with such an account_type
               'account_report' : it's the amount of the related report
               'sum' : it's the sum of the children of this record (aka a 'view' record)'''
        res = {}
        fields = ['credit', 'debit', 'balance']
        for report in reports:
            if report.id in res:
                continue
            res[report.id] = dict((fn, 0.0) for fn in fields)
            if report.type == 'accounts':
                # it's the sum of the linked accounts
                res[report.id]['account'] = self._compute_account_balance(report.account_ids)
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_type':
                # it's the sum the leaf accounts with such an account type
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                res[report.id]['account'] = self._compute_account_balance(accounts)
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_report' and report.account_report_id:
                # it's the amount of the linked report
                res2 = self._compute_report_balance(report.account_report_id)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
            elif report.type == 'sum':
                # it's the sum of the children of this account.report
                res2 = self._compute_report_balance(report.children_ids)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
        return res

    def get_account_lines(self, data):
        lines = []
        account_report = self.env['account.financial.report'].search([('id', '=', data['account_report_id'][0])])
        child_reports = account_report._get_children_by_order()
        res = self.with_context(data.get('used_context'))._compute_report_balance(child_reports)
        if data['enable_filter']:
            comparison_res = self.with_context(data.get('comparison_context'))._compute_report_balance(child_reports)
            for report_id, value in comparison_res.items():
                res[report_id]['comp_bal'] = value['balance']
                report_acc = res[report_id].get('account')
                if report_acc:
                    for account_id, val in comparison_res[report_id].get('account').items():
                        report_acc[account_id]['comp_bal'] = val['balance']

        for report in child_reports:
            vals = {
                'name': report.name,
                'balance': res[report.id]['balance'] * report.sign,
                'type': 'report',
                'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                'account_type': report.type or False,  # used to underline the financial report balances
            }
            if data['debit_credit']:
                vals['debit'] = res[report.id]['debit']
                vals['credit'] = res[report.id]['credit']

            if data['enable_filter']:
                vals['balance_cmp'] = res[report.id]['comp_bal'] * report.sign

            lines.append(vals)
            if report.display_detail == 'no_detail':
                # the rest of the loop is used to display the details of the financial report, so it's not needed here.
                continue

            if res[report.id].get('account'):
                sub_lines = []
                for account_id, value in res[report.id]['account'].items():
                    # if there are accounts to display, we add them to the lines with a level equals to their level in
                    # the COA + 1 (to avoid having them with a too low level that would conflicts with the level of data
                    # financial reports for Assets, liabilities...)
                    flag = False
                    account = self.env['account.account'].browse(account_id)
                    vals = {
                        'name': account.code + ' ' + account.name,
                        'balance': value['balance'] * report.sign or 0.0,
                        'type': 'account',
                        'level': report.display_detail == 'detail_with_hierarchy' and 4,
                        'account_type': account.internal_type,
                    }
                    if data['debit_credit']:
                        vals['debit'] = value['debit']
                        vals['credit'] = value['credit']
                        if not account.company_id.currency_id.is_zero(vals['debit']) or not account.company_id.currency_id.is_zero(vals['credit']):
                            flag = True
                    if not account.company_id.currency_id.is_zero(vals['balance']):
                        flag = True
                    if data['enable_filter']:
                        vals['balance_cmp'] = value['comp_bal'] * report.sign
                        if not account.company_id.currency_id.is_zero(vals['balance_cmp']):
                            flag = True
                    if flag:
                        sub_lines.append(vals)
                lines += sorted(sub_lines, key=lambda sub_line: sub_line['name'])
        return lines

    @api.multi
    def check_report_excel(self):
        self.ensure_one()
        row_data = self.check_report()

        data = row_data.get('data', {})
        accounts_res = self.get_account_lines(data.get('form'))
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet(_('Aged Partner Balance'))
        style_bold_font = workbook.add_format({
            'valign': 'vjustify',
            'bold': True,
            'align': 'center',
            })
        display_account = self.target_move
        row = 1
        col = 0
        tilte = self.account_report_id.name
        worksheet.merge_range(row, col + 4, row, col, _('%s') % tilte, style_bold_font)
        row += 2
        col = 0
        worksheet.merge_range(row, col, row, col + 1, _('Target Moves:'), style_bold_font)
        col += 2
        worksheet.write(row, col, _('Start Date'), style_bold_font)
        col += 1
        worksheet.write(row, col, _(self.date_from))
        row += 1
        col = 0
        if display_account == 'all':
            worksheet.merge_range(row, col, row, col + 1, _('All Entries'))
        if display_account == 'posted':
            worksheet.merge_range(row, col, row, col + 1, _('All Posted Entries'))
        col += 2
        worksheet.write(row, col, _('End Date'), style_bold_font)
        col += 1
        worksheet.write(row, col, _(self.date_to))
        if self.debit_credit:
            col = 0
            row += 2
            worksheet.write(row, col, _('Name'), style_bold_font)
            col += 2
            worksheet.write(row, col, _('Debit'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Credit'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Balance'), style_bold_font)
            for acc in accounts_res:
                level = acc.get('level')
                if level != 0:
                    if level < 3:
                        style_bold = workbook.add_format({'bold': True})
                    if level >= 3:
                        style_bold = workbook.add_format({'bold': False})
                    col = 0
                    row += 1
                    worksheet.write(row, col, acc.get('name'), style_bold)
                    col += 2
                    worksheet.write(row, col, '%.2f' % acc.get('debit'), style_bold)
                    col += 1
                    worksheet.write(row, col, '%.2f' % acc.get('credit'), style_bold)
                    col += 1
                    worksheet.write(row, col, '%.2f' % acc.get('balance'), style_bold)
        if self.enable_filter and not self.debit_credit:
            col = 0
            row += 2
            worksheet.write(row, col, _('Name'), style_bold_font)
            col += 2
            worksheet.write(row, col, _('Balance'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('%s') % self.label_filter, style_bold_font)
            for acc in accounts_res:
                level = acc.get('level')
                if level != 0:
                    if level < 3:
                        style_bold = workbook.add_format({'bold': True})
                    if level >= 3:
                        style_bold = workbook.add_format({'bold': False})
                    col = 0
                    row += 1
                    worksheet.write(row, col, acc.get('name'), style_bold)
                    col += 2
                    worksheet.write(row, col, '%.2f' % acc.get('balance'), style_bold)
                    col += 1
                    worksheet.write(row, col, '%.2f' % acc.get('balance_cmp'), style_bold)
        if not self.enable_filter and not self.debit_credit:
            col = 0
            row += 2
            worksheet.write(row, col, _('Name'), style_bold_font)
            col += 3
            worksheet.write(row, col, _('Balance'), style_bold_font)
            for acc in accounts_res:
                level = acc.get('level')
                if level != 0:
                    if level < 3:
                        style_bold = workbook.add_format({'bold': True})
                    if level >= 3:
                        style_bold = workbook.add_format({'bold': False})
                    col = 0
                    row += 1
                    worksheet.write(row, col, acc.get('name'), style_bold)
                    col += 3
                    worksheet.write(row, col, '%.2f' % acc.get('balance'), style_bold)

        workbook.close()
        file_base = base64.b64encode(fp.getvalue())
        fp.close()

        wiz_id = self.env['sheets.excel.output'].create({'filename': file_base, 'name': 'Financial_report.xls'})
        return {
           'type': 'ir.actions.act_window',
           'res_model': 'sheets.excel.output',
           'view_mode': 'form',
           'view_type': 'form',
           'res_id': wiz_id.id,
           'target': 'new',
        }
