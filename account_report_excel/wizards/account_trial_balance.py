import base64
from io import BytesIO
import xlsxwriter

from odoo import api, fields, models, _


class AccountBalanceReport(models.TransientModel):
    _inherit = "account.common.account.report"
    _name = 'account.balance.report'
    _description = 'Trial Balance Report'

    journal_ids = fields.Many2many('account.journal', 'account_balance_report_journal_rel', 'account_id', 'journal_id', string='Journals', required=True, default=[])

    def _print_report(self, data):
        data = self.pre_print_report(data)
        records = self.env[data['model']].browse(data.get('ids', []))
        return self.env.ref('account_report_excel.action_report_trial_balance').report_action(records, data=data)

    def _get_accounts(self, accounts, display_account):
        """ compute the balance, debit and credit for the provided accounts
            :Arguments:
                `accounts`: list of accounts record,
                `display_account`: it's used to display either all accounts or those accounts which balance is > 0
            :Returns a list of dictionary of Accounts with following key and value
                `name`: Account name,
                `code`: Account code,
                `credit`: total amount of credit,
                `debit`: total amount of debit,
                `balance`: total amount of balance,
        """

        account_result = {}
        # Prepare sql query base on selected parameters from wizard
        tables, where_clause, where_params = self.env['account.move.line']._query_get()
        tables = tables.replace('"', '')
        if not tables:
            tables = 'account_move_line'
        wheres = [""]
        if where_clause.strip():
            wheres.append(where_clause.strip())
        filters = " AND ".join(wheres)
        # compute the balance, debit and credit for the provided accounts
        request = ("SELECT account_id AS id, SUM(debit) AS debit, SUM(credit) AS credit, (SUM(debit) - SUM(credit)) AS balance" +\
                   " FROM " + tables + " WHERE account_id IN %s " + filters + " GROUP BY account_id")
        params = (tuple(accounts.ids),) + tuple(where_params)
        self.env.cr.execute(request, params)
        for row in self.env.cr.dictfetchall():
            account_result[row.pop('id')] = row

        account_res = []
        for account in accounts:
            res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
            currency = account.currency_id and account.currency_id or account.company_id.currency_id
            res['code'] = account.code
            res['name'] = account.name
            if account.id in account_result:
                res['debit'] = account_result[account.id].get('debit')
                res['credit'] = account_result[account.id].get('credit')
                res['balance'] = account_result[account.id].get('balance')
            if display_account == 'all':
                account_res.append(res)
            if display_account == 'not_zero' and not currency.is_zero(res['balance']):
                account_res.append(res)
            if display_account == 'movement' and (not currency.is_zero(res['debit']) or not currency.is_zero(res['credit'])):
                account_res.append(res)
        return account_res

    @api.multi
    def check_report_excel(self):
        self.ensure_one()
        row_data = self.check_report()
        data = row_data.get('data', {})
        data = self.pre_print_report(data)
        display_account = data['form']['display_account']

        accounts = self.env['account.account'].search([])
        accounts_res = self.with_context(
            data['form'].get('used_context', {}))._get_accounts(
                accounts, display_account)

        company = self.env.user.company_id
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet(_('Balance Report'))

        style_bold_border_center = workbook.add_format({
            'text_wrap': 1,
            'valign': 'vjustify',
            'border': True,
            'bold': True,
            'align': 'center',
        })

        style_bold_font_border = workbook.add_format({
            'valign': 'vjustify',
            'bold': True,
            'align': 'center',
            'border': True})

        style_bold_font = workbook.add_format({
            'valign': 'vjustify',
            'bold': True,
            'align': 'center',
            })
        style_font_right = workbook.add_format({
            'align': 'right'
            })
        row = 1
        col = 0
        worksheet.merge_range(row, col + 4, row, col, _('%s: Trial Balance') % company.name, style_bold_font)
        row += 2
        col = 0
        worksheet.write(row, col, _('Display Account'), style_bold_font_border)
        col += 1
        worksheet.write(row, col, _('Start Date'), style_bold_font_border)
        col += 1
        worksheet.write(row, col, _(data['form'].get('date_from', False)) or '')
        col += 1
        worksheet.merge_range(row, col, row, col + 1, _('Target Moves:'), style_bold_border_center)
        row += 1
        col = 0
        if display_account == 'all':
            worksheet.write(row, col, _('All accounts'))
        if display_account == 'movement':
            worksheet.write(row, col, _('With movements'))
        if display_account == 'not_zero':
            worksheet.write(row, col, _('With balance not equal to zero'))

        col += 1
        worksheet.write(row, col, _('End Date'), style_bold_font_border)
        col += 1
        worksheet.write(row, col, _(data['form'].get('date_to', False)) or '')
        col += 1
        if data['form']['target_move'] == 'all':
            worksheet.merge_range(row, col, row, col + 1, _('All Entries'))
        if data['form']['target_move'] == 'posted':
            worksheet.merge_range(row, col, row, col + 1, _('All Posted Entries'))

        col = 0
        row += 2
        worksheet.write(row, col, _('Code'), style_bold_font_border)
        col += 1
        worksheet.write(row, col, _('Name'), style_bold_font_border)
        col += 1
        worksheet.write(row, col, _('Debit'), style_bold_font_border)
        col += 1
        worksheet.write(row, col, _('Credit'), style_bold_font_border)
        col += 1
        worksheet.write(row, col, _('Balance'), style_bold_font_border)

        for acc in accounts_res:
            col = 0
            row += 1
            worksheet.write(row, col, acc.get('code'))
            col += 1
            worksheet.write(row, col, acc.get('name'))
            col += 1
            worksheet.write(row, col, '%.2f' % acc.get('debit'), style_font_right)
            col += 1
            worksheet.write(row, col, '%.2f' % acc.get('credit'), style_font_right)
            col += 1
            worksheet.write(row, col, '%.2f' % acc.get('balance'), style_font_right)
            col += 1
        workbook.close()
        file_base = base64.b64encode(fp.getvalue())
        fp.close()
        wiz_id = self.env['sheets.excel.output'].create({'filename': file_base, 'name': 'Trial_Balance.xls'})
        return {
           'type': 'ir.actions.act_window',
           'res_model': 'sheets.excel.output',
           'view_mode': 'form',
           'view_type': 'form',
           'res_id': wiz_id.id,
           'target': 'new',
        }
