# -*- coding: utf-8 -*-

from odoo import api, fields, models


class Output(models.TransientModel):
    _name = 'sheets.excel.output'
    _description = 'Excel Report Output'

    name = fields.Char('File Name', size=256, readonly=True)
    filename = fields.Binary('File to Download', readonly=True)
    extension = fields.Char('Extension', default='xls')
