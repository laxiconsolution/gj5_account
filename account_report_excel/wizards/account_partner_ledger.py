# -*- coding: utf-8 -*-
import base64
from io import BytesIO
import xlsxwriter
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountPartnerLedger(models.TransientModel):
    _inherit = "account.common.partner.report"
    _name = "account.report.partner.ledger"
    _description = "Account Partner Ledger"

    amount_currency = fields.Boolean("With Currency",
                                     help="It adds the currency column on report if the "
                                          "currency differs from the company currency.")
    reconciled = fields.Boolean('Reconciled Entries')

    def _print_report(self, data):
        data = self.pre_print_report(data)
        data['form'].update({'reconciled': self.reconciled, 'amount_currency': self.amount_currency})
        return self.env.ref('account_report_excel.action_report_partnerledger').report_action(self, data=data)

    def _lines(self, data, partner):
        full_account = []
        currency = self.env['res.currency']
        query_get_data = self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()
        reconcile_clause = "" if data['form']['reconciled'] else ' AND "account_move_line".full_reconcile_id IS NULL '
        params = [partner.id, tuple(data['computed']['move_state']), tuple(data['computed']['account_ids'])] + query_get_data[2]
        query = """
            SELECT "account_move_line".id, "account_move_line".date, j.code, acc.code as a_code, acc.name as a_name, "account_move_line".ref, m.name as move_name, "account_move_line".name, "account_move_line".debit, "account_move_line".credit, "account_move_line".amount_currency,"account_move_line".currency_id, c.symbol AS currency_code
            FROM """ + query_get_data[0] + """
            LEFT JOIN account_journal j ON ("account_move_line".journal_id = j.id)
            LEFT JOIN account_account acc ON ("account_move_line".account_id = acc.id)
            LEFT JOIN res_currency c ON ("account_move_line".currency_id=c.id)
            LEFT JOIN account_move m ON (m.id="account_move_line".move_id)
            WHERE "account_move_line".partner_id = %s
                AND m.state IN %s
                AND "account_move_line".account_id IN %s AND """ + query_get_data[1] + reconcile_clause + """
                ORDER BY "account_move_line".date"""
        self.env.cr.execute(query, tuple(params))
        res = self.env.cr.dictfetchall()
        sum = 0.0
        lang_code = self.env.context.get('lang') or 'en_US'
        lang = self.env['res.lang']
        lang_id = lang._lang_get(lang_code)
        date_format = lang_id.date_format
        for r in res:
            r['date'] = r['date']
            r['displayed_name'] = '-'.join(
                r[field_name] for field_name in ('move_name', 'ref', 'name')
                if r[field_name] not in (None, '', '/')
            )
            sum += r['debit'] - r['credit']
            r['progress'] = sum
            r['currency_id'] = currency.browse(r.get('currency_id'))
            full_account.append(r)
        return full_account

    def _sum_partner(self, data, partner, field):
        if field not in ['debit', 'credit', 'debit - credit']:
            return
        result = 0.0
        query_get_data = self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()
        reconcile_clause = "" if data['form']['reconciled'] else ' AND "account_move_line".full_reconcile_id IS NULL '

        params = [partner.id, tuple(data['computed']['move_state']), tuple(data['computed']['account_ids'])] + query_get_data[2]
        query = """SELECT sum(""" + field + """)
                FROM """ + query_get_data[0] + """, account_move AS m
                WHERE "account_move_line".partner_id = %s
                    AND m.id = "account_move_line".move_id
                    AND m.state IN %s
                    AND account_id IN %s
                    AND """ + query_get_data[1] + reconcile_clause
        self.env.cr.execute(query, tuple(params))

        contemp = self.env.cr.fetchone()
        if contemp is not None:
            result = contemp[0] or 0.0
        return result

    @api.multi
    def check_report_excel(self):
        self.ensure_one()
        row_data = self.check_report()
        data = row_data.get('data', {})
        data = self.pre_print_report(data)
        data['form'].update({'reconciled': self.reconciled, 'amount_currency': self.amount_currency})
        if not data.get('form'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        data['computed'] = {}

        obj_partner = self.env['res.partner']
        query_get_data = self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()
        data['computed']['move_state'] = ['draft', 'posted']
        if data['form'].get('target_move', 'all') == 'posted':
            data['computed']['move_state'] = ['posted']
        result_selection = data['form'].get('result_selection', 'customer')
        if result_selection == 'supplier':
            data['computed']['ACCOUNT_TYPE'] = ['payable']
        elif result_selection == 'customer':
            data['computed']['ACCOUNT_TYPE'] = ['receivable']
        else:
            data['computed']['ACCOUNT_TYPE'] = ['payable', 'receivable']

        self.env.cr.execute("""
            SELECT a.id
            FROM account_account a
            WHERE a.internal_type IN %s
            AND NOT a.deprecated""", (tuple(data['computed']['ACCOUNT_TYPE']),))
        data['computed']['account_ids'] = [a for (a,) in self.env.cr.fetchall()]
        params = [tuple(data['computed']['move_state']), tuple(data['computed']['account_ids'])] + query_get_data[2]
        reconcile_clause = "" if data['form']['reconciled'] else ' AND "account_move_line".full_reconcile_id IS NULL '
        query = """
            SELECT DISTINCT "account_move_line".partner_id
            FROM """ + query_get_data[0] + """, account_account AS account, account_move AS am
            WHERE "account_move_line".partner_id IS NOT NULL
                AND "account_move_line".account_id = account.id
                AND am.id = "account_move_line".move_id
                AND am.state IN %s
                AND "account_move_line".account_id IN %s
                AND NOT account.deprecated
                AND """ + query_get_data[1] + reconcile_clause
        self.env.cr.execute(query, tuple(params))
        partner_ids = [res['partner_id'] for res in self.env.cr.dictfetchall()]
        partners = obj_partner.browse(partner_ids)
        partners = sorted(partners, key=lambda x: (x.ref or '', x.name or ''))

        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet(_('Partner Report'))
        # style_bold_border_center = workbook.add_format({
        #     'text_wrap': 1,
        #     'valign': 'vjustify',
        #     'border': True,
        #     'align': 'center',
        # })

        style_bold_font_border = workbook.add_format({
            'valign': 'vjustify',
            'bold': True,
            'align': 'center',
            'border': True})

        style_bold_font = workbook.add_format({
            'valign': 'vjustify',
            'bold': True,
            'align': 'center',
            })
        style_font_right = workbook.add_format({
            'align': 'right'
            })
        row = 0
        col = 0
        worksheet.merge_range(row, col, row, col + 6, _('Partner Report'), style_bold_font_border)
        row += 1
        worksheet.merge_range(row, col, row, col + 1, _('Date From'), style_bold_font)
        col = 2
        worksheet.write(row, col, _(self.date_from) or '', style_bold_font)
        col += 2
        worksheet.merge_range(row, col, row, col + 2, _('Target Moves'), style_bold_font)

        row += 1
        col = 0
        worksheet.merge_range(row, col, row, col + 1, _('Date To'), style_bold_font)
        col = 2
        worksheet.write(row, col, _(self.date_to) or '', style_bold_font)
        col += 2
        move = ''
        if self.target_move == 'draft':
            move = _('All Entries')
        if self.target_move == 'posted':
            move = _('All Posted Entries')
        worksheet.merge_range(row, col, row, col + 2, move, style_bold_font)

        row += 2
        col = 0
        worksheet.write(row, col, 'Date', style_bold_font_border)
        col += 1
        worksheet.write(row, col, 'Journal', style_bold_font_border)
        col += 1
        worksheet.write(row, col, 'Account', style_bold_font_border)
        col += 1
        worksheet.write(row, col, 'Entry Label', style_bold_font_border)
        col += 1
        worksheet.write(row, col, 'Debit', style_bold_font_border)
        col += 1
        worksheet.write(row, col, 'Credit', style_bold_font_border)
        col += 1
        worksheet.write(row, col, 'Balance', style_bold_font_border)
        row += 1
        col = 0
        for record in partners:
            row += 1
            col = 0
            worksheet.merge_range(row, col, row, col + 2, str(record.ref or '') + '-' + str(record.name), style_bold_font)
            col = 4
            worksheet.write(row, col, '%.2f' % self._sum_partner(data, record, 'debit'), style_bold_font_border)
            col += 1
            worksheet.write(row, col, '%.2f' % self._sum_partner(data, record, 'credit'), style_bold_font_border)
            col += 1
            worksheet.write(row, col, '%.2f' % self._sum_partner(data, record, 'debit - credit'), style_bold_font_border)
            partner_line = self._lines(data, record)
            for line in partner_line:
                row += 1
                col = 0
                worksheet.write(row, col, _(line['date']) or " ")
                col += 1
                worksheet.write(row, col, line['code'] or " ")
                col += 1
                worksheet.write(row, col, line['a_code'] or " ")
                col += 1
                worksheet.write(row, col, line['displayed_name'] or " ")
                col += 1
                worksheet.write(row, col, '%.2f' % line['debit'] or " ", style_font_right)
                col += 1
                worksheet.write(row, col, '%.2f' % line['credit'] or " ", style_font_right)
                col += 1
                worksheet.write(row, col, '%.2f' % line['progress'] or " ")
            row += 2
        workbook.close()
        file_base = base64.b64encode(fp.getvalue())
        fp.close()

        wiz_id = self.env['sheets.excel.output'].create({'filename': file_base, 'name': 'Partner_Ledger.xls'})
        return {
           'type': 'ir.actions.act_window',
           'res_model': 'sheets.excel.output',
           'view_mode': 'form',
           'view_type': 'form',
           'res_id': wiz_id.id,
           'target': 'new',
        }
