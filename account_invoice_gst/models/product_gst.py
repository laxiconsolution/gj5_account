# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import api, fields, models, _


class product_gst(models.Model):
    _name = "product.gst"

    name = fields.Char(string='HSN', required=True, copy=False)
    cgst_sale_tax_id = fields.Many2one('account.tax', string='CGST Sale', required=True)
    sgst_sale_tax_id = fields.Many2one('account.tax', string='SGST Sale', required=True)
    igst_sale_tax_id = fields.Many2one('account.tax', string='IGST Sale', required=True)
    cgst_purchase_tax_id = fields.Many2one('account.tax', string='CGST Purchase', required=True)
    sgst_purchase_tax_id = fields.Many2one('account.tax', string='SGST Purchase', required=True)
    igst_purchase_tax_id = fields.Many2one('account.tax', string='IGST Purchase', required=True)
    product_category_id = fields.Many2one('product.category', string='Product Category')
    company_id = fields.Many2one('res.company', string="Company", default=lambda self: self.env['res.company']._company_default_get())

    # for Tony sir req
    non_gst = fields.Boolean(string="NON GST ?", default=False)
    taxability = fields.Selection([('exempted', 'Exempted'), ('nil_rated', 'NIL Rated'), ('taxable', 'Taxable')], default="taxable", string="Taxability")
    cess_sale_tax_id = fields.Many2one('account.tax', string="CESS Sale")
    cess_purchase_tax_id = fields.Many2one('account.tax', string="CESS Purchase")

    eligible_inp_crd = fields.Boolean(string="Ineligible for Input Credit", default=False)
    itc_clain_type = fields.Selection([('input_service', 'Input Service'), ('input', 'Input'), ('capital_good', 'Capital Good')], string="ITC Clain Type")

    _sql_constraints = [('hsn_key_uniq', 'unique (name)', 'HSN code must be unique!')]

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        categ_id = self.env.context.get('categ_id', False)
        if categ_id:
            self._cr.execute("""
                SELECT
                    id
                FROM
                    product_gst
                WHERE
                    product_category_id = %s
            """ % (categ_id))
            res = self._cr.dictfetchall()
            gst_ids = []
            if res:
                gst_ids = [i['id'] for i in res]
            args += [('id', 'in', gst_ids)]
        return super(product_gst, self)._search(args=args, offset=offset,
                                                limit=limit, order=order,
                                                count=count, access_rights_uid=access_rights_uid)
