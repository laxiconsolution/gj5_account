# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import fields, models


class AccountTax(models.Model):
    _inherit = 'account.tax'

    tax_type = fields.Selection(selection=[('cgst', 'CGST'), ('sgst', 'SGST'), ('igst', 'IGST'), ('cess', 'CESS')], string="GST Tax Type")

    # def init(self):
    #     self.env.cr.execute("""update account_tax set name = CONCAT('Old-', name) where  id in (SELECT id from account_tax where name LIKE '%GST%')""")
    # #     self.env.cr.execute("""DELETE FROM account_tax WHERE tax_type IS NULL""")


class AccountTaxTemplate(models.Model):
    _inherit = 'account.tax.template'

    tax_type = fields.Selection(selection=[('cgst', 'CGST'), ('sgst', 'SGST'), ('igst', 'IGST'), ('cess', 'CESS')], string="GST Tax Type")

    # def init(self):
    #     self.env.cr.execute("""DELETE FROM account_tax_template WHERE tax_type IS NULL""")
    #     self.env.cr.execute("""UPDATE account_tax_template set active ='False' WHERE tax_type IS NULL""")
