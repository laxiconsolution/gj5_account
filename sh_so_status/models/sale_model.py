# -*- coding: utf-8 -*-
# Part of Laxicon Solution.

from odoo import models,fields,api


class sale_order(models.Model):
    _inherit = "sale.order"

    sh_fully_delivered = fields.Boolean(string="Delivered", default=False, copy=False, compute="check_delivery", store=True)
    sh_partially_delivered = fields.Boolean(string="Partially Delivered", default=False, copy=False, compute="check_delivery", store=True)
    sh_fully_paid = fields.Boolean(string="Paid", default=False, copy=False, compute="check_delivery", search="_search_fully_paid")
    sh_partially_paid = fields.Boolean(string="Partially Paid", default=False, copy=False, compute="check_delivery", search="_search_partial_paid")
    sh_hidden_compute_field = fields.Boolean(string="Hidden Compute", compute="check_delivery")

    def _search_partial_paid(self, operator, value):
        so_ids = []
        for so_rec in self.search([]):
            if so_rec.invoice_ids:
                sum_of_invoice_amount = 0.0
                sum_of_due_amount = 0.0
                for invoice_id in so_rec.invoice_ids.filtered(lambda inv: inv.state not in ['cancel', 'draft']):
                    sum_of_invoice_amount = sum_of_invoice_amount + invoice_id.amount_total_signed
                    sum_of_due_amount = sum_of_due_amount + invoice_id.residual_signed
                    if invoice_id.residual_signed != 0 and invoice_id.residual_signed < invoice_id.amount_total_signed:
                        if so_rec.id not in so_ids:
                            so_ids.append(so_rec.id)
                    # down payment
                    if invoice_id.residual_signed == 0 and invoice_id.amount_total_signed < so_rec.amount_total:
                        if so_rec.id not in so_ids:
                            so_ids.append(so_rec.id)

                # down payment
                if sum_of_due_amount == 0 and sum_of_invoice_amount < so_rec.amount_total:
                    if so_rec.id not in so_ids:
                        so_ids.append(so_rec.id)

                if sum_of_due_amount == 0 and sum_of_invoice_amount >= so_rec.amount_total:
                    if so_rec.id in so_ids:
                        so_ids.remove(so_rec.id)
        return [('id', 'in', so_ids)]

    def _search_fully_paid(self, operator, value):
        so_ids = []
        for so_rec in self.search([]):
            if so_rec.invoice_ids:
                sum_of_invoice_amount = 0.0
                sum_of_due_amount = 0.0
                for invoice_id in so_rec.invoice_ids.filtered(lambda inv: inv.state not in ['cancel', 'draft']):
                    sum_of_invoice_amount = sum_of_invoice_amount + invoice_id.amount_total_signed
                    sum_of_due_amount = sum_of_due_amount + invoice_id.residual_signed
                if sum_of_due_amount == 0 and sum_of_invoice_amount >= so_rec.amount_total:
                    so_ids.append(so_rec.id)
        return [('id', 'in', so_ids)]

    @api.multi
    @api.depends('order_line.qty_delivered')
    def check_delivery(self):
        # enhancement - fix for Molicc parent component
        if self:
            for so_rec in self:
                print('so_rec.state=' + str(so_rec.state))
                if so_rec.state == 'sale':
                    so_rec.sh_hidden_compute_field = False
                    if so_rec.order_line and type(so_rec.id) == int:
                        no_service_product_line = so_rec.order_line.filtered(lambda line: (line.product_id) and (line.product_id.type != 'service'))
                        if no_service_product_line:
                            so_rec.sh_partially_delivered = False
                            so_rec.sh_fully_delivered = False
                            product_uom_qty = qty_delivered = 0
                            # for line in no_service_product_line:
                            #     qty_delivered += line.qty_delivered
                            #     product_uom_qty += line.product_uom_qty
                            #     # enhancement - fix for Molicc parent component
                            #     if line.parent_product and not line.sub_component:
                            #         product_uom_qty -= line.product_uom_qty
                            #         print('qty_delivered=' + str(qty_delivered))
                            #         print('product_uom_qty=' + str(product_uom_qty))
                            if product_uom_qty == qty_delivered:
                                so_rec.sh_fully_delivered = True
                            elif product_uom_qty > qty_delivered and qty_delivered != 0.0:
                                so_rec.sh_partially_delivered = True
                    if so_rec.invoice_ids:
                        sum_of_invoice_amount = 0.0
                        sum_of_due_amount = 0.0
                        so_rec.sh_fully_paid = False
                        so_rec.sh_partially_paid = False
                        for invoice_id in so_rec.invoice_ids.filtered(lambda inv: inv.state not in ['cancel', 'draft']):
                            sum_of_invoice_amount = sum_of_invoice_amount + invoice_id.amount_total_signed
                            sum_of_due_amount = sum_of_due_amount + invoice_id.residual_signed
                            if invoice_id.residual_signed != 0 and invoice_id.residual_signed < invoice_id.amount_total_signed:
                                so_rec.sh_partially_paid = True
                        if sum_of_due_amount == 0 and sum_of_invoice_amount >= so_rec.amount_total:
                            so_rec.sh_fully_paid = True

