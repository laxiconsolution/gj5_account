
# -*- coding: utf-8 -*-
# Part of Laxicon Solution.
{
    "name": "Sale Order Status",
    "author": "Laxicon Solution",
    "website": "https://www.laxicon.in",
    "support": "info@laxicon.in",
    "category": "Sales",
    "summary": """sale order delivery module, filter sale order invoice, so partial delivery app, find full paid amount in so, partial so paid amount status, invoice status odoo""",
    "description": """
    This module useful to get status of delivery and invoices of sale orders. Easily filters sale orders with delivered, partial delivered, paid, partially paid.


    12.0.2 (bug fixed)
    ======
    #update in v12 we need to check if record is saved or not in order to write.
    #type(so_rec.id) == int if true then record are stored in db(ready to write) else record did not saved yet.
    if so_rec.order_line and type(so_rec.id) == int:
    - copy = False added in each 4 fields""",
    "version": "12.0.4",
    "depends": ["base", "sale", "sale_management", "stock", "account"],
    "application": True,
    "data": ['views/sale_view.xml'],
    "images": ["static/description/background.png"],
    "live_test_url": "https://youtu.be/X_eOGGJWrfY",
    "auto_install": False,
    "installable": True,
    "price": 13,
    "currency": "EUR"
}
