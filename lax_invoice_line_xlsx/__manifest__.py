{
    'name': "Invoice Line Xlsx Report",
    'summary': """ This module allows to xlsx report of multiple Invoice Line from the tree/form view.""",
    'author': "Laxicon Solution",
    'website': "www.laxicon.in",
    'sequence': 101,
    'support': 'info@laxicon.in',
    'category': 'Account',
    'version': '12.0.1',
    'license': 'LGPL-3',
    'description': """This module allows to Xlsx Report of Invoice Line from the tree/form view.
    """,
    'depends': ['account', 'report_xlsx'],
    'data': [
        "report/invoice_line_xlsx.xml",
    ],
    'images':  ["static/description/banner.png"],
    'installable': True,
    'auto_install': False,
    'application': True,
    "pre_init_hook":  "pre_init_check",
}
