from odoo import models


class InvoiceLineXlsx(models.AbstractModel):
    _name = 'report.lax_invoice_line_xlsx.report_invoice_line_xlsx'
    _inherit = 'report.report_xlsx.abstract'
    _description = "Invoice Line Xlsx"

    def generate_xlsx_report(self, workbook, data, invoice_line):
        sheet = workbook.add_worksheet("Invoice Lines")
        bold = workbook.add_format({'bold': True})
        date_format = workbook.add_format({'num_format': 'dd/mm/yy'})
        align_center = workbook.add_format({'align': 'center', 'bold': True})
        floating_point_bordered = workbook.add_format({'num_format': '#,##0.00'})
        floating_point_bold_bordered = workbook.add_format({'num_format': '#,##0.00', 'bold': True})
        row = 0
        col = 0
        sheet.merge_range(row, col, row, col + 13, 'Invoice Line Data', align_center)
        row += 2
        col = 0
        header_list = ['Reference', 'Customer', 'Invoice Date', 'Due Date', 'Salesperson', 'Product', 'HSN Code', 'Description', 'Quantity', 'Price',
                       'Taxes', 'Tax Amount', 'Subtotal w/o Tax', 'Total']
        for h_data in header_list:
            sheet.write(row, col, h_data, bold)
            col += 1
        row += 1
        col = 0
        tax_sumry = []
        for obj in invoice_line:
            for record in obj.invoice_line_ids:
                if record.price_unit > 0.0:
                    tx_amount = 0.0
                    tx_list = []
                    for tx in record.invoice_line_tax_ids:
                        tx_list.append(tx.name)
                        tx_amount += tx.amount
                sheet.write(row, col, record.invoice_id.number)
                col += 1
                sheet.write(row, col, record.invoice_id.partner_id.name)
                col += 1
                sheet.write(row, col, record.invoice_id.date_invoice, date_format)
                col += 1
                sheet.write(row, col, record.invoice_id.date_due, date_format)
                col += 1
                sheet.write(row, col, record.invoice_id.user_id.name)
                col += 1
                sheet.write(row, col, record.product_id.name)
                col += 1
                sheet.write(row, col, '997326')
                col += 1
                sheet.write(row, col, record.name)
                col += 1
                sheet.write(row, col, record.quantity)
                col += 1
                sheet.write(row, col, record.price_unit)
                col += 1
                sheet.write(row, col, ', '.join(tx_list))
                col += 1
                tax_data = ((record.price_subtotal * tx_amount) / 100) / 2
                sheet.write(row, col, (tax_data * 2), floating_point_bordered)
                col += 1
                sub_total = record.price_subtotal
                sheet.write(row, col, sub_total)
                tax_per = 0.0
                if tx_amount > 0:
                    tax_per = tx_amount / 2
                tax_key = ("CGST" + '@' + "%s") % tax_per
                tax_dict = {'base_amount': record.price_subtotal, 'tax_amount': tax_data}
                tax_sumry.append({tax_key: tax_dict})
                tax_key = ("SGST" + '@' + "%s") % tax_per
                tax_dict = {'base_amount': record.price_subtotal, 'tax_amount': tax_data}
                tax_sumry.append({tax_key: tax_dict})
                col += 1
                sub_with_tax = record.price_subtotal + (2 * tax_data)
                sheet.write(row, col, sub_with_tax, floating_point_bordered)
                col = 0
                row += 1
                tx_list.clear()
                col = 0
            col = 0
        row += 3
        col = 0
        tax_header_list = ['Tax Name', 'Tax Amount', 'Base Amount']
        for rec in tax_header_list:
            sheet.write(row, col, rec, bold)
            col += 1
        row += 1
        col = 0
        key_list = []
        for s in tax_sumry:
            for k, v in s.items():
                if k not in key_list:
                    key_list.append(k)
        final_list = []
        for key in key_list:
            tax_amount = 0.0
            base_amount = 0.0
            for d in tax_sumry:
                for k, v in d.items():
                    if k == key:
                        tax_amount += v.get('tax_amount')
                        base_amount += v.get('base_amount')
            final_list.append({key: {'base_amount': base_amount, 'tax_amount': tax_amount}})
        total_tx = 0.0
        for rec in final_list:
            for k, v in rec.items():
                sheet.write(row, col, k + '%')
                col += 1
                b = v.get('tax_amount')
                sheet.write(row, col, round(b, 2))
                total_tx += b
                col += 1
                a = v.get('base_amount')
                sheet.write(row, col, round(a, 2))
                row += 1
                col = 0
        row += 0
        col = 1
        sheet.write(row, col, total_tx, floating_point_bold_bordered)
        workbook.close()
